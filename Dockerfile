ARG NGINX_IMAGE
ARG NODE_IMAGE
ARG VUE_APP_API_HOST
ARG VUE_APP_GOOGLE_ANALYTICS_ID

FROM ${NODE_IMAGE} AS app

ARG VUE_APP_API_HOST
ARG VUE_APP_GOOGLE_ANALYTICS_ID

USER root

COPY app/ /app/

RUN chown -R gitlab-runner:gitlab-runner /app

USER gitlab-runner

ENV VUE_APP_API_HOST=$VUE_APP_API_HOST
ENV VUE_APP_GOOGLE_ANALYTICS_ID=$VUE_APP_GOOGLE_ANALYTICS_ID

RUN npm install \
    && npm run build

#--------------------------------------------------------
# NGINX
#--------------------------------------------------------

FROM registry.gitlab.com/dia-helper/dia-helper-frontend/nginx:4032178c AS nginx

COPY docker/nginx/conf/default.prod.conf /etc/nginx/conf.d/default.conf
COPY --from=app --chown=nginx:nginx /app/dist /app/public/
