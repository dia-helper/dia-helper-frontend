import ru from '@/locales/Ru'
import en from '@/locales/En'
import ua from '@/locales/Ua'

export default class Translator {
    private static languageList: { [k: string]: { [k: string]: string } } = {
        'ru': ru,
        'en': en,
        'ua': ua,
    }
    public static localize = function (key: string): string {
        const language: string = localStorage.language || 'en'
        return Translator.languageList[language][key] || `[Localize error]: key ${key} not found`
    }
}
