export default {
    "GlucoseChart": "Графік цукрів",
    "Home": "На головну",
    "About": "Про сайт",
    "ChartGenerate": "Сформувати графік",
    "Generate": "Сформувати",
    "ChooseFile": "Виберіть файл",
    "NoFileSelected": "Файл не вибраний",
    "FileSelected": "Файл вибрано",
    "FromDate": "З",
    "ToDate": "До",
    "HypoglycemicThreshold": "Поріг гіпоглікемії",
    "HyperglycemiaThreshold": "Поріг гіперглікемії",
    "AverageGlucose": "Середній рівень глюкози",
    "LowGlucosePercent": "Низький",
    "HighGlucosePercent": "Високий",
    "InGlucosePercent": "В діапазоні",
    "SelectTheUploadFileFormat": "Виберіть формат файла вивантаження",
    "LoadDataFile": "Завантажити файл з даними",
    "ExportToFile": "Експорт в файл",
    "Error": "Помилка",
    "BadSize": "Розмір файла повинен бути від 1 байта до 10 Мегабайт",
    "BadFormat": "Формат файла некоректний для данного типу вивантаження",
    "DefaultErrorMessage": "Щось пішло не так",
    "ContactUs": "Напишіть нам",
    "Name": "Ім'я",
    "Email": "Email",
    "Message": "Повідомлення",
    "NamePlaceholder": "Введіть ім'я",
    "EmailPlaceholder": "Введіть email",
    "MessagePlaceholder": "Введіть повідомлення",
    "Send": "Відправити",
    "SendMessage": "Відправити повідомлення",
    "feedbackSent": "Повідомлення відправлено",
    "Success": "Успішно",
    "FeedbackLink": "Форма зворотнього зв'язку ",
    "AboutPageHead": "Про наш ресурс",
    "AboutPageDescription": "Цей сайт призначений для того, щоб зробити життя кожного діабетика кращим. На даний момент на нашому ресурсі ви можете побудувати графік цукрів, використовуючи вивантаження з різних пристроїв та програм, для подальшого аналізу даних або надання їх вашому лікарю:",
    "AboutPageHomeLink": "побудувати графік цукрів",
    "AboutPageAddFormat": "Список форматів файлів, що підтримуються, поповнюється завдяки зворотному зв'язку від користувачів. Якщо у вас є пристрій або програма, обробка файлів з яких не підтримується нашим ресурсом, ми будемо вдячні, якщо ви вишлете нам даний файл вивантаження з описом через",
    "AboutPageErrorReporting": "Сайт розробляється на чистому ентузіазмі, про всі помилки, неточності та побажання прохання також повідомляти через форму зворотного зв'язку.",
    "AttentionTitle": "УВАГА!",
    "AboutPageAttentionContent": "Ресурс знаходиться у стадії \"beta\". Не варто покладатися на інформацію, одержану вами тут, на 100%. Ми вдосконалюємось.",
    "AboutPageEnd": "Здоров'я вам і рівних цукрів",
    "AssetsPath": "ua",
    "XdrippCsvInstructionHead": "Вивантаження csv-файла з xDrip+",
    "XdrippCsvInstructionMainWindow": "У головному вікні програми натискаємо три точки у правому верхньому кутку екрана.",
    "XdrippCsvInstructionMainMenu": "У відкрившомуся меню вибираємо пункт \"Імпорт/Експорт даних\".",
    "XdrippCsvInstructionExportMenu": "У меню варіантів формату вивантаження вибираємо \"Експорт CSV (формат SiDiary)\".",
    "XdrippCsvInstructionDateStartForm": "У календарі, що відкрився, вибираємо початкову дату тимчасового проміжку, за який нам необхідно вивантажити дані. Зверніть увагу, дані завжди вивантажуються до сьогоднішнього дня включно.",
    "XdrippCsvInstructionDateStartApply": "Натискаємо кнопку \"OK\".",
    "XdrippCsvInstructionExportPathNotification": "На екрані з'явиться повідомлення із зазначенням шляху до файлу вивантаження.",
    "XdrippSqliteInstructionHead": "Вивантаження sqlite-файла з xDrip+",
    "XdrippSqliteInstructionMainWindow": "У головному вікні програми натискаємо три точки у правому верхньому кутку екрана.",
    "XdrippSqliteInstructionMainMenu": "У меню вибираємо пункт \"Імпорт/Експорт даних\".",
    "XdrippSqliteInstructionExportMenu": "У меню варіантів формату вивантаження вибираємо \"Експорт бази даних\".",
    "XdrippSqliteInstructionExportPathNotification": "На екрані з'явиться повідомлення із зазначенням шляху до файлу вивантаження.",
    "XdrippSqliteInstructionAttentionContent": "Розмір файлу бази SQLite може бути дуже великим. На даний момент максимальний розмір файлу, який можна завантажити на сайт, дорівнює 10 Мегабайт. Якщо файл sqlite перевищує це обмеження, спробуйте вивантажити файл csv за наступною інструкцією:",
    "XdrippCsvInstructionLinkText": "інструкція з вивантаження файлу csv з Xdrip+",
    "OneTouchRevealCsvInstructionHead": "Вивантаження csv-файлу з OneTouch Reveal",
    "OneTouchRevealCsvInstructionMainWindow": "У головному вікні програми натискаємо кнопку з іконкою \"Поділитися\".",
    "OneTouchRevealCsvInstructionExportMenu": "У розділі \"Export Data\" вибираємо формат \"CSV\".",
    "OneTouchRevealCsvInstructionExportParams": "Тиснемо на кнопку \"Time Frame\" для відкриття вікна вибору періоду.",
    "OneTouchRevealCsvInstructionPeriodSelect": "У відкрившомуся вікні вибираємо цікавий для нас проміжок часу.",
    "OneTouchRevealCsvInstructionOpenIn": "Натискаємо на кнопку \"Open in...\" для вибору програми, в якій ми хочемо відкрити файл. З цього ж вікна ми можемо відправити файл собі на електронну пошту (кнопка \"Email\".)",
    "OneTouchRevealCsvInstructionAppSelect": "Вибираємо програму, в якій буде відкрито csv-файл для збереження.",
    "UnloadingInstruction": "Інструкція з вивантаження",
    "NotFoundText": "Такої сторінки не знайдено. Якщо ви вважаєте, що сталася помилка на сайті, напишіть нам про це, будь ласка, через:",
    "EmptyDataError": "Відсутні дані для відображення",
    "GlimpCsvInstructionHead": "Вивантаження csv.gz-файлу з Glimp",
    "GlimpCsvInstructionMainWindow": "У головному вікні програми натискаємо три точки у правому верхньому кутку екрана.",
    "GlimpCsvInstructionMainMenu": "У меню вибираємо пункт \"Налашт\".",
    "GlimpCsvInstructionSettings": " Вибираємо \"Cloud\" у меню налаштувань.",
    "GlimpCsvInstructionCloudSettings": "У розділі \"Dropbox\" натискаємо на перший елемент \"Dropbox\".",
    "GlimpCsvInstructionDropboxAllowGlimp": "У вікні браузера, що відкрилося, натискаємо кнопку \"Дозволити\", щоб дати Glimp можливість зберігати дані у вашу папку Dropbox.",
    "GlimpCsvInstructionFileInDropbox": "Тепер отримуючи дані про цукор Glimp збереже інформацію в каталозі Програми/Glimp у вашій папці Dropbox. Щоб створити графік, завантажте на наш сайт файл GlicemiaMisurazioni.csv.gz.",
    "GlimpCsvInstructionCopyLocalFile": "Ви можете просто скопіювати файл з пам'яті вашого смартфона (або флеш-карти) по шляху \"/Android/data/it.ct.glicemia/files/Documents/GlicemiaMisurazioni.csv.gz\" АБО можете зробити дії за наступною інструкцією для отримання файлу \"GlicemiaMisurazioni.csv.gz\" у вашій папці Dropbox.",
    "HelpMapAbout": "На цій карті зазначені координати та контакти людей, готових допомогти у скрутну хвилину людям із цукровим діабетом: поділитися інсуліном, засобами діагностики, розхідниками; дати корисну пораду.",
    "HelpMapAddContacts": "Якщо ви також могли б допомогти людям з діабетом, які потрапили в складну життєву ситуацію, залиште, будь ласка, свої контакти на цій карті.",
    "HelpMapAddContactsButton": "Можу допомогти",
    "HelpMapRemoveContacts": "Якщо ви бажаєте видалити свої дані з карти, натисніть, будь ласка, на кнопку нижче.",
    "HelpMapRemoveContactsButton": "Видалити свої дані",
    "AddHelperEnterData": "Перемістіть курсор на місце вашого розташування та вкажіть свої контактні дані.",
    "AddHelperEnterName": "Як до вас можна звертатися:",
    "AddHelperEnterPhone": "Номер вашого телефону (тільки цифри):",
    "EnterSmsCode": "Введіть код із смс:",
    "AddHelperSuccess": "Дані успішно додані",
    "SpecifyLocation": "Вкажіть розташування",
    "SendConfirmationCode": "Надіслати код підтвердження",
    "Phone": "Телефон",
    "RemoveHelperConfirmDeletion": "Щоб підтвердити видалення контактів із карти взаємодопомоги, вкажіть, будь ласка, номер вашого телефону (тільки цифри):",
    "RemoveHelperSuccessfully": "Ваші дані успішно видалено",
    "HelperMap": "Карта допомоги",
    "Timezone": "Часовий пояс",
    "NightscoutPageOffer": "Якщо у вас виникла необхідність створення сайту Nightscout для роботи з системою моніторингу глюкози, ми можемо вам у цьому допомогти.",
    "NightscoutPageFreeMonth": "Ви можете використовувати сервер безкоштовно протягом місяця.",
    "NightscoutPageSituationsList": "Якщо коротко, то Nightscout може бути корисним у різних ситуаціях:",
    "NightscoutPageSituationAndroidAPS": "для проходження цілей при налаштуванні AndroidAPS;",
    "NightscoutPageSituationRemoteControl": "для віддаленого моніторингу за цукром іншої людини, наприклад вашої дитини;",
    "NightscoutPageSituationReport": "для побудови різних звітів та графіків зі статистикою;",
    "NightscoutPageSituationBrowser": "для спостереження за своїм цукром у браузері на комп'ютері або мобільному пристрої;",
    "NightscoutPageTerms": "Умови надання послуги:",
    "NightscoutPageTermsFreeMonth": "Один місяць користування безкоштовно.",
    "NightscoutPageTermsLocation": "Розташування сервера в оптимальному регіоні.",
    "NightscoutPageTermsInit": "Можливе початкове налаштування сервера з нуля.",
    "NightscoutPageTermsSavingParams": "Можливе збереження всіх параметрів з вашого поточного сервера, наприклад Heroku.",
    "NightscoutPageTermsSavingData": "Можливе збереження всіх даних з вашої бази даних, розташованої, наприклад, в Atlas MongoDB.",
    "NightscoutPageTermsBeautifulURL": "Симпатична адреса вашого nightscout-сайту виду: ВАШЕ_ІМ'Я.diahelper.com. Основний сайт також діабетичної тематики.",
    "NightscoutPageDB": "Можна повністю встановити Nightscout і базу даних MongoDB на новий сервер, так і підключити новий Nightscout до вже існуючої бази даних в Atlas, якщо, з якихось причин, хочеться залишити поточну базу даних.",
    "NightscoutPageServiceCancellation": "Після першого місяця безкоштовного використання можливим відмова від послуги з вашого боку без жодного пояснення причин і без будь-яких претензій у зв'язку з цим з нашого боку.",
    "NightscoutPagePrice": "З другого місяця використання ціна становитиме $5 на місяць.",
    "NightscoutPageQuestions": "Якщо у вас залишилися питання або ви хочете скористатися пропозицією, напишіть нам у месенджерах чи соціальних мережах за кнопками вгорі сторінки або через",
    "ChartFileScale": "Якість зображення (0,1 - 10)",
    "Donate": "Підтримати проект",
}
