import {ServiceInterface} from "@/services/ServiceInterface";

export class ServiceFactory {
    public static readonly xdrippCsvServiceName = 'xdripp-csv'
    public static readonly xdrippSqliteServiceName = 'xdripp-sqlite'
    public static readonly oneTouchRevealCsvServiceName = 'one-touch-reveal-csv'
    public static readonly glimpCsvServiceName = 'glimp-csv'

    public static readonly serviceTypeList = [
        {
            'typeName': ServiceFactory.xdrippCsvServiceName,
            'acceptFileFormat': '.zip',
            'label': 'xDrip+ csv | *.zip',
            'instructionRouteName': 'XdrippCsvInstruction',
            'representation': 'xDrip+ csv (zip)',
        },
        {
            'typeName': ServiceFactory.xdrippSqliteServiceName,
            'acceptFileFormat': '.zip',
            'label': 'xDrip+ sqlite | *.zip',
            'instructionRouteName': 'XdrippSqliteInstruction',
            'representation': 'xDrip+ SQLite (zip)',
        },
        {
            'typeName': ServiceFactory.oneTouchRevealCsvServiceName,
            'acceptFileFormat': '.csv',
            'label': 'OneTouch Reveal | *.csv',
            'instructionRouteName': 'OneTouchRevealCsvInstruction',
            'representation': 'OneTouch Reveal csv',
        },
        {
            'typeName': ServiceFactory.glimpCsvServiceName,
            'acceptFileFormat': '.gz',
            'label': 'Glimp | *.csv.gz',
            'instructionRouteName': 'GlimpCsvInstruction',
            'representation': 'Glimp csv.gz',
        }
    ]

    public static createFromName(name: string): ServiceInterface | undefined {
        return this.serviceTypeList.find(x => x.typeName === name)
    }
}
