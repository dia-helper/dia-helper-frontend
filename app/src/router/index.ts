import Vue from 'vue'
import VueRouter, {RouteConfig} from 'vue-router'
import Home from '../views/Home.vue'
import ChartPage from '../components/ChartPage.vue'
import DataLoader from "@/components/DataLoader.vue";
import {ServiceFactory} from "@/services/ServiceFactory";

Vue.use(VueRouter)

const mainRouteList: Array<RouteConfig> = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
        path: '/feedback',
        name: 'Feedback',
        component: () => import('../components/Feedback.vue')
    },
    {
        path: '/chart-main',
        name: 'ChartMain',
        component: () => import('../views/ChartMain.vue')
    },
    {
        path: '/helper-map',
        name: 'HelperMap',
        component: () => import('../views/HelperMap.vue')
    },
    {
        path: '/add-helper-form',
        name: 'AddHelperForm',
        component: () => import('../components/AddHelperForm.vue')
    },
    {
        path: '/remove-helper-form',
        name: 'RemoveHelperForm',
        component: () => import('../components/RemoveHelperForm.vue')
    },
    {
        path: '/nightscout',
        name: 'Nightscout',
        component: () => import('../views/Nightscout.vue')
    },
    {
        path: '/:serviceType',
        name: 'DataLoader',
        component: DataLoader,
        props: true,
        beforeEnter: (to, from, next) => {
            if (ServiceFactory.createFromName(to.params.serviceType) == undefined) next({ name: 'NotFound' })
            else next()
        }
    },
    {
        path: '/:serviceType/chart-page',
        name: 'ChartPage',
        component: ChartPage,
        props: true,
        beforeEnter: (to, from, next) => {
            if (to.params.rawData == undefined) next({ name: 'DataLoader', params: {serviceType: to.params.serviceType} })
            else next()
        }
    },
]

const instructionRouteList: Array<RouteConfig> = []
ServiceFactory.serviceTypeList.forEach(
    (serviceItem) => {
        instructionRouteList.push(
            {
                path: '/' + serviceItem.typeName + '/instruction',
                name: serviceItem.instructionRouteName,
                component: () => import('../views/instruction/' + serviceItem.instructionRouteName + '.vue')
            }
        )
    }
)

const systemRouteList: Array<RouteConfig>  = [
    {
        path: '/404',
        name: 'NotFound',
        component: () => import('../views/NotFoundPage.vue')
    },
    {
        path: '*',
        redirect: '/404'
    },
]

const routes = mainRouteList.concat(instructionRouteList, systemRouteList)
const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
