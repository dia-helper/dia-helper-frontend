export class RawDataDto {
    constructor(public readonly labels: number[], public readonly data: number[]) {}
}
