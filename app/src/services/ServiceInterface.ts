export interface ServiceInterface {
    typeName: string;
    acceptFileFormat: string;
    label: string;
    instructionRouteName: string;
    representation: string;
}