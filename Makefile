init:
	docker-compose down --remove-orphans
	cp app/.env app/.env.local
	docker-compose up -d
	docker-compose exec app npm install

restart:
	docker-compose down --remove-orphans
	docker-compose up -d