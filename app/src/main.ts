import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Translator from './services/Translator'
import VueGtag from "vue-gtag";
import L from 'leaflet'

type D = L.Icon.Default & {
    _getIconUrl?: string;
}
delete (L.Icon.Default.prototype as D)._getIconUrl
L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
})

Vue.config.productionTip = false

Vue.filter('localize', Translator.localize)

if (process.env.NODE_ENV === 'production') {
    Vue.use(VueGtag, {
        config: { id: process.env.VUE_APP_GOOGLE_ANALYTICS_ID }
    }, router);
}

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
